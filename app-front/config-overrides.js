const {
  addLessLoader,
  fixBabelImports,
  override
} = require('customize-cra');
const rewireLess = require('react-app-rewire-less');

module.exports = {
  webpack: override(
    addLessLoader({
      javascriptEnabled: true
    }),
    fixBabelImports('babel-plugin-import', {
      libraryName: 'antd',
      style: true
    }),
    rewireLess.withLoaderOptions({
      modifyVars: {
          "@layout-body-background": "#FFFFFF",
          "@layout-header-background": "#FFFFFF",
          "@layout-footer-background": "#FFFFFF" 
      },
      javascriptEnabled: true
    })
  )
};