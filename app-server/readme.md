## Etapas para configurar o aplicativo de back end do Spring Boot (app-server)

1. **Clone Aplicação**

	bash
	git clone https://hancharekmartins@bitbucket.org/hancharekmartins/testpost.git
	
	cd testpost
	```
2. **Criar MySQL database**

	```bash
	create database polling_app
	```

3. **
Altere o nome de usuário e a senha do MySQL conforme sua instalação do MySQL**

	+ abra `src/main/resources/application.properties` file.

	+ selecione `spring.datasource.username` e `spring.datasource.password` propriedades de sua instalação do mysql

4. **Run the app**

	Você pode executar o aplicativo de inicialização de mola digitando o seguinte comando -

	```bash
	mvn spring-boot:run
	```

	O servidor será iniciado na porta 8080.

Você também pode empacotar o aplicativo na forma de um arquivo `jar` e então executá-lo como -

	```bash
	mvn package
	java -jar target/polls-0.0.1-SNAPSHOT.jar
	```
5. **Default Roles**
	
	O spring boot app usa role baseado autorização spring security. Para adicionar o padrão roles em sua database, Eu adicionei as seguintes consultas sql em `src/main/resources/data.sql` arquivo. Spring boot
irá executar automaticamente este script na inicialização -

	```sql
	INSERT IGNORE INTO roles(name) VALUES('ROLE_USER');
	INSERT IGNORE INTO roles(name) VALUES('ROLE_ADMIN');
	```

	Qualquer novo usuário que se inscrever no aplicativo é atribuído ao `ROLE_USER` por default.

## Etapas para configurar o aplicativo front end React (app-client)


Primeiro vá para pasta `app-client` -

```bash
cd app-client
```

Em seguida, digite o seguinte comando para instalar as dependências e iniciar o aplicativo -

```bash
npm install && npm start
```

O servidor front-end será iniciado na porta `3000`.
